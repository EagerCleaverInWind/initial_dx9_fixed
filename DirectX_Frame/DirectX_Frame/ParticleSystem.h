#pragma  once

#include "Utility.h"
#include "CameraClass.h"
#include <list>


struct Particle
{
	D3DXVECTOR3		pisition;
	D3DCOLOR			color;
	static const DWORD FVF=D3DFVF_XYZ| D3DFVF_DIFFUSE;
};

struct Attribute
{
	D3DXVECTOR3		_position;
	D3DXVECTOR3		_velocity;
	D3DXVECTOR3		_acceleration;
	D3DXCOLOR		_color;
	D3DXCOLOR		_colorFade;
	float		_lifetime;
	float		_age;
	bool		_isAlive;
};


class PSystem
{
protected:
	LPDIRECT3DDEVICE9					m_pDevice;
	LPDIRECT3DVERTEXBUFFER9		m_pVertexBuf;
	LPDIRECT3DTEXTURE9				m_pTex;

	D3DXVECTOR3		m_iniPos;
	BoundingBox			m_bouBox;

	float  m_addRate;
	float  m_ParSize;
	int		m_maxParticles;
	std::list<Attribute>		m_Particles;

	DWORD	m_vbSize;
	DWORD	m_vbOffset;
	DWORD	m_vbBatchSize;


protected:
	virtual void RemoveDeadParticles();


public:
	PSystem();
	virtual ~PSystem();

	virtual bool Init(LPDIRECT3DDEVICE9 device,wchar_t *texFileName);
	virtual void ResetParticle(Attribute *attribute)=0;
	virtual void Reset();
	virtual void AddParticle();

	virtual void Update(float timeDelta)=0;

	virtual void PreRender();
	virtual void Render(D3DMATRIX *pMatWorld=NULL);
	virtual void PostRender();

	bool IsEmpty() {return m_Particles.empty(); }
	bool	IsDead();
};



class Snow:public PSystem
{
public:
	Snow(BoundingBox *boundingBox,int numParticles);
	void ResetParticle(Attribute *attribute);
	void Update(float timeDelta);
};


class FireWork:public PSystem
{
public:
	FireWork(D3DXVECTOR3 *origin,int numParticles);
	void ResetParticle(Attribute *attribute);
	void Update(float timeDelta);
	//���أ�
	void PreRender();
	void PostRender();
};


class ParticleGun:public PSystem
{
private:
	Camera  *m_pCamera;
	D3DXCOLOR		m_LaserColor;
public:
	ParticleGun(Camera *camera,D3DXCOLOR laserColor);
	void ResetParticle(Attribute *attribute);
	void Update(float timeDelta);
};



