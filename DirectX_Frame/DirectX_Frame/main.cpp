
//-----------------------------------【引用头文件】---------------------------------------
#include <time.h>

#include "Utility.h"
#include "DirectInputClass.h"
//------------------------------------------------------------------------------------------------    

//-----------------------------------【引用库文件】---------------------------------------
#pragma comment(lib,"winmm.lib")//调用PlaySound函数所需库文件
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")
//------------------------------------------------------------------------------------------------    

//-----------------------------------【宏定义】---------------------------------------
#define  WINDOW_WIDTH 800
#define  WINDOW_HEIGHT 600
#define  WINDOW_TITTLE L"hehe"
#define  WNDCLASS_NAME L"haha"
//------------------------------------------------------------------------------------------------    

//-----------------------------------【全局变量声明】---------------------------------------
LPDIRECT3DDEVICE9  g_pd3dDevice=NULL;
DInput* g_pDInput=NULL;
LPD3DXFONT g_pFont=NULL;
//------------------------------------------------------------------------------------------------    

//-----------------------------------【函数声明】---------------------------------------
LRESULT  CALLBACK  WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam);
HRESULT  Direct3D_Init(HWND hwnd);
LRESULT  Object_Init(HINSTANCE hInstance,HWND hwnd);
void Direct3D_Update(float timeDelta);
void  Direct3D_Render(HWND hwnd,float timeDelta);
void  Direct3D_CleanUp();
//------------------------------------------------------------------------------------------------    

//-----------------------------------【测试用】---------------------------------------

//------------------------------------------------------------------------------------------------    


int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nShowCmd)
{
	WNDCLASSEX wndClass={0};
	wndClass.cbSize=sizeof(WNDCLASSEX);
	wndClass.style=CS_HREDRAW|CS_VREDRAW;
	wndClass.lpfnWndProc=WndProc;
	wndClass.cbClsExtra=0;
	wndClass.cbWndExtra=0;
	wndClass.hInstance=hInstance;
	wndClass.hIcon=(HICON)::LoadImage(NULL,L"icon.ico",IMAGE_ICON,0,0,LR_DEFAULTSIZE|LR_LOADFROMFILE);
	wndClass.hCursor=LoadCursor(NULL,IDC_ARROW);
	wndClass.hbrBackground=(HBRUSH)GetStockObject(GRAY_BRUSH);
	wndClass.lpszMenuName=NULL;
	wndClass.lpszClassName=WNDCLASS_NAME;

	if(!RegisterClassEx(&wndClass))
		return -1;

	HWND hwnd=CreateWindow(WNDCLASS_NAME,WINDOW_TITTLE,WS_OVERLAPPEDWINDOW,0,0,
		WINDOW_WIDTH,WINDOW_HEIGHT,NULL,NULL,hInstance,NULL);
	MoveWindow(hwnd,250,80,WINDOW_WIDTH,WINDOW_HEIGHT,true);
	ShowWindow(hwnd,nShowCmd);
	UpdateWindow(hwnd);

	//PlaySound(L"音频文件名.wav",NULL,SND_FILENAME|SND_ASYNC|SND_LOOP);

	Direct3D_Init(hwnd);
	Object_Init(hInstance,hwnd);


	MSG msg={0};
	__int64 cntsPerSec=0;
	QueryPerformanceFrequency((LARGE_INTEGER*)&cntsPerSec);
	float secsPerCnt=1.0f/(float)cntsPerSec;
	__int64 prevCnts,currentCnts;
	float dt;
	QueryPerformanceCounter((LARGE_INTEGER*)&prevCnts);
	while (msg.message!=WM_QUIT)
	{
		if (PeekMessage(&msg,0,0,0,PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			QueryPerformanceCounter((LARGE_INTEGER*)&currentCnts);
			dt=(currentCnts-prevCnts)*secsPerCnt;
			Direct3D_Update(dt);
			Direct3D_Render(hwnd,dt);
			prevCnts=currentCnts;
		}
	}

	UnregisterClass(WNDCLASS_NAME,wndClass.hInstance);
	return 0;
}


LRESULT  CALLBACK  WndProc(HWND hwnd,UINT message,WPARAM wParam,LPARAM lParam)
{
	switch (message)
	{
	case   WM_KEYDOWN:
		if(wParam==VK_ESCAPE)
			DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		Direct3D_CleanUp();
		PostQuitMessage(0);
		break;
		//-----------------------------------【测试用】---------------------------------------

		//------------------------------------------------------------------------------------------------    

	default:
		return DefWindowProc(hwnd,message,wParam,lParam);
	}

	return 0;
}

HRESULT  Direct3D_Init(HWND hwnd)
{
	//创建D3D接口
	LPDIRECT3D9		PD3D=NULL;
	if(NULL==(PD3D=Direct3DCreate9(D3D_SDK_VERSION)))
		return E_FAIL;

	//获取硬件设备信息
	D3DCAPS9 caps;
	int vp=0;
	if(FAILED(PD3D->GetDeviceCaps(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,&caps) ) )
		return E_FAIL;
	if(caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
		vp=D3DCREATE_HARDWARE_VERTEXPROCESSING;
	else
		vp=D3DCREATE_SOFTWARE_VERTEXPROCESSING;

	//填充D3DPRESENT_PARAMETERS结构体
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp,sizeof(d3dpp));
	d3dpp.BackBufferWidth=WINDOW_WIDTH;
	d3dpp.BackBufferHeight=WINDOW_HEIGHT;
	d3dpp.BackBufferFormat=D3DFMT_A8R8G8B8;
	d3dpp.BackBufferCount=1;
	d3dpp.MultiSampleType=D3DMULTISAMPLE_NONE;
	d3dpp.MultiSampleQuality=0;
	d3dpp.SwapEffect=D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow=hwnd;
	d3dpp.Windowed=true;
	d3dpp.EnableAutoDepthStencil=true;
	d3dpp.AutoDepthStencilFormat=D3DFMT_D24S8;
	d3dpp.Flags=0;
	d3dpp.FullScreen_RefreshRateInHz=0;
	d3dpp.PresentationInterval=D3DPRESENT_INTERVAL_IMMEDIATE;

	//创建D3D设备
	if(FAILED(PD3D->CreateDevice(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL,hwnd,vp,&d3dpp,&g_pd3dDevice) ) )
		return E_FAIL;

	SAFE_RELEASE(PD3D);
	return S_OK;
}


LRESULT  Object_Init(HINSTANCE hInstance,HWND hwnd)
{
	srand((unsigned int)time(0));

	g_pDInput=new DInput;
	g_pDInput->Init(hInstance,hwnd);

	D3DXCreateFont(g_pd3dDevice, 28, 0, 1000, 0, false, DEFAULT_CHARSET, 
		OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, 0, L"微软雅黑", &g_pFont); 

	//-----------------------------------【测试用】---------------------------------------

	//------------------------------------------------------------------------------------------------    


	//投影变换
	D3DXMATRIX matProj;
	D3DXMatrixPerspectiveFovLH(&matProj, D3DX_PI / 4.0f,(float)((double)WINDOW_WIDTH/WINDOW_HEIGHT),1.0f, 2000.0f);
	g_pd3dDevice->SetTransform(D3DTS_PROJECTION, &matProj);


	return S_OK;
}

void Direct3D_Update(float timeDelta)
{
	//g_pDInput->GetInput();
	//-----------------------------------【测试用】---------------------------------------

	//------------------------------------------------------------------------------------------------    

}

void  Direct3D_Render( HWND hwnd,float timeDelta)
{
	g_pd3dDevice->Clear(0,NULL,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,D3DCOLOR_XRGB(0,0,0),1.0f,0);//最开始没有添加D3DCLEAR_ZBUFFER的清理，静态还好，动态的时候老是出现逐渐被黑色吞没的问题……快哭了
	g_pd3dDevice->BeginScene();

	//-----------------------------------【测试用】---------------------------------------

	//------------------------------------------------------------------------------------------------    

	ShowFPS(g_pFont,D3DCOLOR_XRGB(180,96,96),WINDOW_WIDTH,WINDOW_HEIGHT,timeDelta,true);

	g_pd3dDevice->EndScene();
	g_pd3dDevice->Present(NULL,NULL,NULL,NULL);

	ScreenShot(g_pd3dDevice);
}


void  Direct3D_CleanUp()
{

	SAFE_RELEASE(g_pd3dDevice);
}




