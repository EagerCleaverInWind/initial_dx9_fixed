#include "GUI.h"

GUISystem::GUISystem(LPDIRECT3DDEVICE9  device,int w,int h)
{
	m_ppFonts=NULL;
	m_pControls=NULL;
	m_ppVertexBuffers=NULL;
	m_totalFonts=m_totalBuffers=m_totalControls=0;
	m_ifUseBackDrop=false;

	m_pd3dDevice=device;
	m_windowWidth=w;m_windowHeight=h;
	memset(&m_backDrop,0,sizeof(GUIControl));
}


GUISystem::~GUISystem()
{
	Shutdown();
}


bool	GUISystem::CreateFont(wchar_t *fontName,int size,int *fontId)
{
	if(!m_pd3dDevice)		return false;
	if(!m_ppFonts)
	{
		m_ppFonts=new LPD3DXFONT[1];
	}
	else
	{
		LPD3DXFONT		*temp=new LPD3DXFONT[m_totalFonts+1];
		memcpy(temp,m_ppFonts,sizeof(LPD3DXFONT )* m_totalFonts);
		delete[]		m_ppFonts;
		m_ppFonts=temp;
	}
	D3DXCreateFont(m_pd3dDevice,size,0,0,1,0,DEFAULT_CHARSET,OUT_DEFAULT_PRECIS,DEFAULT_QUALITY,DEFAULT_PITCH|FF_DONTCARE,
		fontName,&m_ppFonts[m_totalFonts]);

	if(!m_ppFonts[m_totalFonts])		return false;
	*fontId=m_totalFonts;
	m_totalFonts++;

	return true;
}


bool GUISystem::IncreaseControl()
{
	if(!m_totalControls)
	{
		m_pControls=new GUIControl[1];
	}
	else
	{
		GUIControl *temp=new GUIControl[m_totalControls+1];
		if(!temp)	return false;
		memcpy(temp,m_pControls,sizeof(GUIControl)*m_totalControls);
		delete[]		m_pControls;
		m_pControls=temp;
	}

	m_pControls[m_totalControls].m_text=NULL;
	m_pControls[m_totalControls].m_backDrop=NULL;
	m_pControls[m_totalControls].m_downTex=NULL;
	m_pControls[m_totalControls].m_overTex=NULL;
	m_pControls[m_totalControls].m_upTex=NULL;
	//这一步非常有必要，因为并没有默认初始化NULL，而不同控件类型是有选择地使用成员变量，到时候释放内存时如果没用到的成员指针却不是NULL……
	return true;
}


bool	GUISystem::IncreaseBuffer()
{
	if(!m_ppVertexBuffers)
	{
		m_ppVertexBuffers=new LPDIRECT3DVERTEXBUFFER9[1];
	}
	else
	{
		LPDIRECT3DVERTEXBUFFER9	*temp=new LPDIRECT3DVERTEXBUFFER9[m_totalBuffers+1];
		if(!temp)	return false;
		memcpy(temp,m_ppVertexBuffers,sizeof(LPDIRECT3DVERTEXBUFFER9)*m_totalBuffers);
		delete[]		m_ppVertexBuffers;
		m_ppVertexBuffers=temp;
	}
	return true;
}


bool	GUISystem::AddBackDrop(wchar_t *fileName)
{
	if(!fileName)		return false;

	m_backDrop.m_type=GUI_BACKDROP;
	if(D3DXCreateTextureFromFile(m_pd3dDevice,fileName,&m_backDrop.m_backDrop)!=D3D_OK)	return false;

	float w=(float)m_windowWidth;
	float h=(float)m_windowHeight;
	GUIVertex	 obj[]=
	{
		{w,0,0.0f,1,D3DCOLOR_XRGB(255,255,255),1.0f,0.0f},
		{w,h,0.0f,1,D3DCOLOR_XRGB(255,255,255),1.0f,1.0f},
		{0,0,0.0f,1,D3DCOLOR_XRGB(255,255,255),0.0f,0.0f},
		{0,h,0.0f,1,D3DCOLOR_XRGB(255,255,255),0.0f,1.0f}
	};
	if(m_pd3dDevice->CreateVertexBuffer(sizeof(obj),0,D3DFVF_GUI,D3DPOOL_DEFAULT,&m_pBackDropBuffer,NULL)!=D3D_OK)
		return false;
	void *ptr;
	m_pBackDropBuffer->Lock(0,sizeof(obj),(void **)&ptr,0);
	memcpy(ptr,obj,sizeof(obj));
	m_pBackDropBuffer->Unlock();

	m_ifUseBackDrop=true;

	return true;
}


bool	GUISystem::AddStaticText(int id,wchar_t *text,float x,float y,D3DCOLOR col,int fontID)
{
	if(!text||fontID<0||!m_pd3dDevice)	return false;
	if(!IncreaseControl())							return false;
	m_pControls[m_totalControls].m_type=GUI_STATICTEXT;
	m_pControls[m_totalControls].m_id=id;
	m_pControls[m_totalControls].m_color=col;
	m_pControls[m_totalControls].m_xPos=x;
	m_pControls[m_totalControls].m_yPos=y;
	m_pControls[m_totalControls].m_listId=fontID;
	
	int len=wcslen(text);
	m_pControls[m_totalControls].m_text=new wchar_t[len+1];
	memcpy(m_pControls[m_totalControls].m_text,text,len*2);//因为是根据字节而非个数来复制，所以宽字符型个数*2
	m_pControls[m_totalControls].m_text[len]='\0';

	m_totalControls++;
	
	return true;

}


bool	GUISystem::AddButton(int id,float x,float y,wchar_t *up,wchar_t *over,wchar_t *down)
{
	if(!m_pd3dDevice||!up||!over||!down)		return false;
	IncreaseControl();
	m_pControls[m_totalControls].m_id=id;
	m_pControls[m_totalControls].m_xPos=x;
	m_pControls[m_totalControls].m_yPos=y;
	D3DXCreateTextureFromFile(m_pd3dDevice,up,&m_pControls[m_totalControls].m_upTex);
	D3DXCreateTextureFromFile(m_pd3dDevice,over,&m_pControls[m_totalControls].m_overTex);
	D3DXCreateTextureFromFile(m_pd3dDevice,down,&m_pControls[m_totalControls].m_downTex);

	D3DSURFACE_DESC		desc;
	m_pControls[m_totalFonts].m_upTex->GetLevelDesc(0,&desc);
	float w=(float)desc.Width;
	float h=(float)desc.Height;
	m_pControls[m_totalFonts].m_width=w;
	m_pControls[m_totalFonts].m_height=h;
	D3DXCOLOR		white=D3DCOLOR_XRGB(255,255,255);
	GUIVertex obj[]=
	{
		{x+w,y+0,0.0f,1,white,1.0f,0.0f},
		{x+w,y+h,0.0f,1,white,1.0f,1.0f},
		{x+0,y+0,0.0f,1,white,0.0f,0.0f},
		{x+0,y+h,0.0f,1,white,0.0f,1.0f}
	};

	if(!IncreaseBuffer())		return false;
	if(m_pd3dDevice->CreateVertexBuffer(sizeof(obj),0,D3DFVF_GUI,D3DPOOL_DEFAULT,&m_ppVertexBuffers[m_totalBuffers],NULL)!=D3D_OK)
		return false;
	void *ptr;
	if(FAILED(m_ppVertexBuffers[m_totalBuffers]->Lock(0,sizeof(obj),(void**)&ptr,0)))	return false;
	memcpy(ptr,obj,sizeof(obj));
	m_ppVertexBuffers[m_totalBuffers]->Unlock();
	m_pControls[m_totalControls].m_listId=m_totalBuffers;
	m_totalBuffers++;

	m_totalControls++;

	return true;
}


void GUISystem::Shutdown()
{
	if(m_ifUseBackDrop)
	{
		if(m_backDrop.m_backDrop)		m_backDrop.m_backDrop->Release();
		if(m_pBackDropBuffer)			m_pBackDropBuffer->Release();
	}
	m_backDrop.m_backDrop=NULL;
	m_pBackDropBuffer=NULL;

	for(int i=0;i<m_totalFonts;i++)
	{
		if(m_ppFonts[i])
		{
			m_ppFonts[i]->Release();
			m_ppFonts[i]=NULL;
		}
	}
	if(m_ppFonts)
	{
		delete[]		m_ppFonts;
		m_ppFonts=NULL;
	}

	for(int i=0;i<m_totalBuffers;i++)
	{
		if(m_ppVertexBuffers[i])
		{
			m_ppVertexBuffers[i]->Release();
			m_ppVertexBuffers[i]=NULL;
		}
	}
	if(m_ppVertexBuffers)
	{
		delete[]		m_ppVertexBuffers;
		m_ppVertexBuffers=NULL;
	}

	for(int i=0;i<m_totalControls;i++)
	{
		if(m_pControls[i].m_upTex)//一开始没有让不需要用到的成员指针初始化为NULL，结果关闭时 0xC0000005内存泄漏
		{
			m_pControls[i].m_upTex->Release();
			m_pControls[i].m_upTex=NULL;
		}
		if(m_pControls[i].m_downTex)
		{
			m_pControls[i].m_downTex->Release();
			m_pControls[i].m_downTex=NULL;
		}
		if(m_pControls[i].m_overTex)
		{
			m_pControls[i].m_overTex->Release();
			m_pControls[i].m_overTex=NULL;
		}
		if(m_pControls[i].m_text)
		{
			delete[]		m_pControls[i].m_text;
			m_pControls[i].m_text=NULL;
		}
	}
	if(m_pControls)
	{
		delete[]		m_pControls;
		m_pControls=NULL;
	}

	//我擦！一个非常严重的错误：D3D设备传进来，其他地方也要用，应该由创建它的这一级释放，而不能在这里就释放掉
	//if(m_pd3ddevice)
	//	m_pd3ddevice->release();
	//m_pd3ddevice=null;
	//更新：上面的，你错了，不同于一次性的delete，COM组件内部采用的是引用计数器机制
	//再次更新：还是不该Release，接口指针间的赋值是不会自动调用AddRef()的
}



void		ProcessGUI(GUISystem *gui,bool LMBDown,int mouseX,int mouseY,void (*funcPtr)(int id,int state))
{
	if(!gui)		return;

	LPDIRECT3DDEVICE9		device=gui->GetD3DDevice();
	if(!device)	return;
	GUIControl	*pControl;
	LPDIRECT3DVERTEXBUFFER9		pVertexbuffer;
	LPD3DXFONT		pfont;
	RECT	fontPosition={0,	0,	(long)gui->GetWindowWidth(),	(long)gui->GetWindowHeight() };
	int status=GUI_BUTTON_UP;


	if(gui->IfUseBackDrop())
	{
		pControl=gui->GetBackDrop();
		pVertexbuffer=gui->GetBackDropBuffer();

		device->SetTexture(0,pControl->m_backDrop);
		device->SetStreamSource(0,pVertexbuffer,0,sizeof(GUIVertex));
		device->SetFVF(D3DFVF_GUI);
		device->DrawPrimitive(D3DPT_TRIANGLESTRIP,0,2);
	}

	device->SetRenderState(D3DRS_ALPHABLENDENABLE,true);
	device->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
	device->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_DESTALPHA);

	for(int i=0;i<gui->GetTotalControls();i++)
	{
		pControl=gui->GetGUIControl(i);
		switch (pControl->m_type)
		{
		case GUI_STATICTEXT:
			fontPosition.left=pControl->m_xPos;
			fontPosition.top=pControl->m_yPos;
			pfont=gui->GetFont(pControl->m_listId);
			pfont->DrawTextW(NULL,pControl->m_text,-1,&fontPosition,DT_LEFT,pControl->m_color);
			break;


		case GUI_BUTTON:
			pVertexbuffer=gui->GetVertexBuffer(pControl->m_listId);
			if(mouseX>pControl->m_xPos&&mouseX<pControl->m_xPos+pControl->m_width
				&&mouseY>pControl->m_yPos&&mouseY<pControl->m_yPos+pControl->m_height)
			{
				if(LMBDown)
				{
					device->SetTexture(0,pControl->m_downTex);
					status=GUI_BUTTON_DOWN;
				}
				else	device->SetTexture(0,pControl->m_overTex);

			}
			else
			{
				device->SetTexture(0,pControl->m_upTex);
			}
			device->SetStreamSource(0,pVertexbuffer,0,sizeof(GUIVertex));
			device->SetFVF(D3DFVF_GUI);
			device->DrawPrimitive(D3DPT_TRIANGLESTRIP,0,2);

			if(funcPtr)		funcPtr(i,status);

			break;


		default:
			break;
		}
	}
	device->SetRenderState(D3DRS_ALPHABLENDENABLE,FALSE);
}
