#pragma  once

#include <d3d9.h>
#include <d3dx9.h>
#include <tchar.h>

#define  SAFE_RELEASE(p) { if(p)		{ (p)->Release();		(p)=NULL; }  }
#define  SAFE_DELETE(p) { if(p)		{ delete(p);	(p)=NULL; }  }

//“żnamespace

DWORD FtoDw(float f);

float GetRandomFloat(float lowBound,float highBound);
void  GetRandomVector(D3DXVECTOR3 * out,D3DXVECTOR3  *min,D3DXVECTOR3  *max);

struct BoundingBox
{
	bool IfPointInside(D3DXVECTOR3 &p);

	D3DXVECTOR3 _min;
	D3DXVECTOR3 _max;
};
struct BoundingSphere
{

	D3DXVECTOR3		_center;
	float _radius;
};


void	ScreenShot(LPDIRECT3DDEVICE9 d3dDevice);

void ShowFPS(LPD3DXFONT	pFont,D3DCOLOR fpsColor,int windowWidth,int windowHeight,float _dt,bool _ifShowMspf=false);

void GetBillboardMatrix(const D3DXMATRIX &matView,D3DXMATRIX &matBillboard);
