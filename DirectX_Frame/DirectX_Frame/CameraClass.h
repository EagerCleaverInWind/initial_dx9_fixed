
#pragma once

#include "Utility.h"

#define  SCREEN_WIDTH		800
#define  SCREEN_HEIGHT		600


class	 Camera
{
	enum  CameraType{ LANDOBJECT,		AIRCRAFT };

private:
	CameraType				m_Type;
	D3DXVECTOR3			m_up;
	D3DXVECTOR3			m_look;
	D3DXVECTOR3			m_right;
	D3DXVECTOR3			m_position;
	D3DXMATRIX			m_ViewMatrix;//取景变换矩阵
	D3DXMATRIX			m_ProjMatrix;//投影变换矩阵
	LPDIRECT3DDEVICE9	m_pd3dDevice;

	D3DXPLANE m_FrustumPlanes[6]; // [0] = near,[1] = far,[2] = left,[3] = right,[4] = top,[5] = bottom

private:
	void	CalculateViewMatrix();
	void UpdateWorldFrustumPlanes();


public:
	void	SetPosition(D3DXVECTOR3	new_position)	{m_position=new_position;}
	void   SetType(CameraType new_type)		{m_Type=new_type;}

	void	MoveAlongUpVec( float units) {m_position+=m_up*units; }
	void	MoveAlongLookVec( float units) {m_position+=m_look*units; }
	void	MoveAlongRightVec( float units) {m_position+=m_right*units; }

	void	 RotationUpVec(float angle);
	void	 RotationLookVec(float angle);
	void	 RotationRightVec(float angle);

	void	Update();//变动完记得最后更新一下
	
	void	SetViewProj(D3DXMATRIX	*pMatrix=NULL);

	void	GetPosition(D3DXVECTOR3 *out) { *out=m_position; }
	void	GetLookDirection(D3DXVECTOR3 *out) {*out=m_look;}

	bool IsVisible(const BoundingBox &box);
public:
	Camera(LPDIRECT3DDEVICE9	pd3dDevice,	CameraType type=LANDOBJECT);
	~Camera();

};