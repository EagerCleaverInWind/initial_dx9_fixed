#pragma  once

#include "Utility.h"

struct Ray
{
	D3DXVECTOR3		 _origin;
	D3DXVECTOR3		_direction;
};

class Pick
{
private:
	LPDIRECT3DDEVICE9	m_pd3dDevice;
	D3DXVECTOR3		m_ViewCoordinateP;
	Ray m_ViewCoordinateRay;
	Ray m_WorldCoordinateRay;

private:
	void TransformRay();

public:
	Pick(LPDIRECT3DDEVICE9 device)
	{
		m_pd3dDevice=device;

		m_ViewCoordinateRay._origin=D3DXVECTOR3(0.0f,0.0f,0.0f);
		m_ViewCoordinateP.z=1.0f;
	}
	~Pick()
	{

	}

	void ComputeBoundingSphereFromMesh(ID3DXMesh *mesh,D3DXVECTOR3 *center,float *radius);
	void ComputeBoundingBoxFromMesh(ID3DXMesh *mesh,D3DXVECTOR3 *min,D3DXVECTOR3 *max);

	void UpdatePointP(float x,float y);

	bool RaySphereIntersectDetection(BoundingSphere *sphere);
	bool RayBoxIntersectDetection(BoundingBox *box);
};