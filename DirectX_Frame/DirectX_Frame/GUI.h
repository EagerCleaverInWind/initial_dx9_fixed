#pragma once

#include "Utility.h"


//支持的GUI控件类型
#define  GUI_STATICTEXT			1
#define GUI_BUTTON					2
#define GUI_BACKDROP				3

//mouse button states
#define  GUI_BUTTON_UP			1
#define  GUI_BUTTON_OVER		2
#define GUI_BUTTON_DOWN		3

struct GUIVertex
{
	float x,	y,	z,	rhw;
	D3DCOLOR	color;
	float u,	v;
};
#define D3DFVF_GUI		(D3DFVF_XYZRHW|D3DFVF_DIFFUSE|D3DFVF_TEX1)


struct GUIControl 
{
	int		m_type;
	int		m_id;
	D3DCOLOR	m_color;
	int		m_listId;
	float	m_xPos,m_yPos;
	float	m_width,m_height;
	wchar_t	*m_text;

	LPDIRECT3DTEXTURE9	m_backDrop;
	LPDIRECT3DTEXTURE9	m_upTex, m_downTex, m_overTex;
};

class GUISystem
{
private:
	LPDIRECT3DDEVICE9	m_pd3dDevice;
	LPD3DXFONT		*m_ppFonts;
	GUIControl		*m_pControls;
	LPDIRECT3DVERTEXBUFFER9		*m_ppVertexBuffers;
	GUIControl		m_backDrop;
	LPDIRECT3DVERTEXBUFFER9		m_pBackDropBuffer;

	bool		m_ifUseBackDrop;
	int			m_totalFonts;
	int			m_totalControls;
	int			m_totalBuffers;

	int			m_windowWidth;
	int			m_windowHeight;

private:
	bool		IncreaseControl();
	bool		IncreaseBuffer();


public:
	GUISystem(LPDIRECT3DDEVICE9  device,int w,int h);
	~GUISystem();

	bool	CreateFont(wchar_t *fontName,int size,int *fontId);
	bool	AddBackDrop(wchar_t *fileName);
	bool	AddStaticText(int id,wchar_t *text,float x,float y,D3DCOLOR col,int fontID);
	bool	AddButton(int id,float x,float y,wchar_t *up,wchar_t *over,wchar_t *down);

	void Shutdown();

	LPDIRECT3DDEVICE9	GetD3DDevice()		{return m_pd3dDevice;}
	GUIControl*					GetBackDrop()		{return &m_backDrop;}
	LPDIRECT3DVERTEXBUFFER9		GetBackDropBuffer()		{return m_pBackDropBuffer;}
	LPD3DXFONT		GetFont(int id)	{ return		m_ppFonts[id];}
	GUIControl *			GetGUIControl(int id)	{ return		&m_pControls[id];}
	LPDIRECT3DVERTEXBUFFER9		GetVertexBuffer(int id)	{return		m_ppVertexBuffers[id];}
	int		GetTotalFonts()			{return m_totalFonts;}
	int		GetTotalControls()		{return m_totalControls;}
	int		GetTotalBuffers()		{return m_totalBuffers;}
	int		GetWindowWidth()		{return m_windowWidth;}
	int		GetWindowHeight()	{return m_windowHeight;}
	bool	IfUseBackDrop()	{return m_ifUseBackDrop;}
	
	void SetWindowSize(int w,int h)		{m_windowWidth=w;m_windowHeight=h;}

};

void		ProcessGUI(GUISystem *gui,bool LMBDown,int mouseX,int mouseY,void (*funcPtr)(int id,int state));