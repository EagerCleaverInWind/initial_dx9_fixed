#pragma once

#include "Utility.h"

#define STRAIGHT_PATH 0
#define CURVE_PATH 1

class Path
{
public:
	Path();
	Path(int type,D3DXVECTOR3 start,D3DXVECTOR3 control1,D3DXVECTOR3 control2,D3DXVECTOR3 end);
	~Path();

	void SetPath(int type,D3DXVECTOR3 start,D3DXVECTOR3 control1,D3DXVECTOR3 control2,D3DXVECTOR3 end);
	void ShutDown();

	int m_type;		//感觉相比宏定义，用枚举类型逼格更高一点？！

	D3DXVECTOR3		m_StartPos;
	D3DXVECTOR3		m_Control1;
	D3DXVECTOR3		m_Control2;
	D3DXVECTOR3		m_EndPos;

	float		m_StartLenth;	//之前路程的总长
	float		m_TotalLenth;	//这段路径的总长

	Path		*m_pNext;
};


class Route
{
private:
	Path	*m_pPath;
	
public:

	float m_StartTime;

	Route();
	~Route();

	bool	AddPath(int type,D3DXVECTOR3 start,D3DXVECTOR3 control1,D3DXVECTOR3 control2,D3DXVECTOR3 end);
	void	GetPosition(float  journey,D3DXVECTOR3 &objPos);

	D3DXVECTOR3 CalcBezierCurvePos(D3DXVECTOR3 start,D3DXVECTOR3 control1,D3DXVECTOR3 control2,D3DXVECTOR3 end,float scalar);
	D3DXVECTOR3 CalcStriaghtPos(D3DXVECTOR3 start,D3DXVECTOR3 end,float scalar);

	void ShutDown();
};


