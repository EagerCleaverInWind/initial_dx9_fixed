
#include "DirectInputClass.h"

DInput::DInput():m_pDirectInput(NULL),m_pKeyboardDevice(NULL),m_pMouseDevice(NULL)//初始化列表
{
	ZeroMemory(m_keyBuffer,sizeof(m_keyBuffer));
	ZeroMemory(&m_MouseState,sizeof(m_MouseState));
}


void	DInput::Init(HINSTANCE hInstance, HWND hwnd)
{
	DirectInput8Create(hInstance,DIRECTINPUT_VERSION,IID_IDirectInput8,(LPVOID*)&m_pDirectInput,NULL);
	m_pDirectInput->CreateDevice(GUID_SysKeyboard, &m_pKeyboardDevice,NULL);
	m_pDirectInput->CreateDevice(GUID_SysMouse, &m_pMouseDevice,NULL);

	m_pKeyboardDevice->SetDataFormat(&c_dfDIKeyboard);
	m_pKeyboardDevice->SetCooperativeLevel(hwnd,DISCL_FOREGROUND|DISCL_EXCLUSIVE);

	m_pMouseDevice->SetDataFormat(&c_dfDIMouse);
	m_pMouseDevice->SetCooperativeLevel(hwnd,DISCL_FOREGROUND|DISCL_EXCLUSIVE);
}


void	DInput::GetInput()
{
	ZeroMemory(m_keyBuffer,sizeof(m_keyBuffer));
	ZeroMemory(&m_MouseState,sizeof(m_MouseState));

	m_pKeyboardDevice->Acquire();
	m_pKeyboardDevice->Poll();
	m_pKeyboardDevice->GetDeviceState(sizeof(m_keyBuffer),m_keyBuffer);

	m_pMouseDevice->Acquire();//鼠标会隐藏
	m_pMouseDevice->Poll();
	m_pMouseDevice->GetDeviceState(sizeof(m_MouseState),&m_MouseState);
}



DInput::~DInput()
{
	m_pKeyboardDevice->Unacquire();
	m_pKeyboardDevice->Release();

	m_pMouseDevice->Unacquire();
	m_pMouseDevice->Release();

	m_pDirectInput->Release();
}

