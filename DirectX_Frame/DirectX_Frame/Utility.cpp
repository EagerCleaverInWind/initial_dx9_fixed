#include "Utility.h"

DWORD FtoDw(float f)
{
	return	*((DWORD*)&f);
}

float GetRandomFloat(float lowBound,float highBound)
{
	if(lowBound>highBound)
	{
		float temp;
		temp=highBound;
		highBound=lowBound;
		lowBound=temp;
	}
	float f=(rand()%10000)*0.0001f;
	return f*(highBound-lowBound)+lowBound;
}
void  GetRandomVector(D3DXVECTOR3 * out,D3DXVECTOR3  *min,D3DXVECTOR3  *max)
{
	out->x=GetRandomFloat(min->x,max->x);
	out->y=GetRandomFloat(min->y,max->y);
	out->z=GetRandomFloat(min->z,max->z);
}

bool BoundingBox::IfPointInside(D3DXVECTOR3 &p)
{
	if(p.x <=_max.x  &&p.y <=_max.y  &&p.z <=_max.z
		&&p.x >=_min.x  &&p.y >=_min.y  &&p.z >=_min.z)
		return true;
	else
		return false;
}


void	ScreenShot(LPDIRECT3DDEVICE9 d3dDevice)//待改进，截图频率太快了
{
	if(GetKeyState(VK_F9)&0x80)
	{
		LPDIRECT3DSURFACE9	surface=NULL;
		//D3DDISPLAYMODE			display;
		//m_pd3dDevice->GetDisplayMode(0,&display);
		//m_pd3dDevice->CreateOffscreenPlainSurface(display.Width,display.Height,D3DFMT_A8R8G8B8,D3DPOOL_DEFAULT,&surface,NULL);
		d3dDevice->GetBackBuffer(0,0,D3DBACKBUFFER_TYPE_MONO,&surface);
		static int	screenshots=1;
		if(screenshots==1)
		{
			CreateDirectory(_T("MyScreenShots"),NULL);
		}
		wchar_t screenshotName[50];
		swprintf_s(screenshotName,_T("MyScreenShots/%d.jpg"),screenshots);
		D3DXSaveSurfaceToFile(screenshotName,D3DXIFF_JPG,surface,NULL,NULL);
		screenshots++;
		if(surface)
		{
			surface->Release();
			surface=NULL;
		}
	}
}

void ShowFPS(LPD3DXFONT	pFont,D3DCOLOR fpsColor,int windowWidth,int windowHeight,float _dt,bool _ifShowMspf)
{
	//calculate
	static float fps=0;
	static float mspf=0;//milliseconds per frame
	static int frameCount=0;
	static float totalTime=0;
	totalTime+=_dt;
	++frameCount;
	if(totalTime>1.0f)
	{
		fps=frameCount/totalTime;
		mspf=1000.0f/fps;
		frameCount=0;
		totalTime=0;
	}

	//show
	static wchar_t	strFps[50];
	RECT  formatRect={0,0,windowWidth,windowHeight};
	//如果用GetClientRect(hwnd,&formatRect);改变窗口大小反而不能自适应，固定长度能自适应是windows内部机制（DefWindowProc）的关系么…
	int charCount=_ifShowMspf?swprintf_s(strFps,25,_T("fps:%0.3f\nmsps:%0.3f"),fps,mspf):swprintf_s(strFps, 20,_T("FPS:%0.3f"),fps);
	pFont->DrawTextW(NULL,strFps,charCount,&formatRect,DT_TOP | DT_RIGHT, fpsColor);


}

void GetBillboardMatrix(const D3DXMATRIX &matView,D3DXMATRIX &matBillboard)
{
	D3DXMatrixIdentity(&matBillboard);
	matBillboard._11=matView._11;
	matBillboard._13=matView._13;
	matBillboard._31=matView._31;
	matBillboard._33=matView._33;
	D3DXMatrixInverse(&matBillboard,NULL,&matBillboard);

}

