#include "Route.h"

Path::Path():m_type(0),m_StartLenth(0),m_TotalLenth(0),m_pNext(NULL)
{

}

Path::Path(int type,D3DXVECTOR3 start,D3DXVECTOR3 control1,D3DXVECTOR3 control2,D3DXVECTOR3 end)
{
	m_StartLenth=0;
	m_TotalLenth=0;
	m_pNext=NULL;
	SetPath(type,start,control1,control2,end);
}

Path::~Path()
{
	ShutDown();
}

void Path::SetPath(int type,D3DXVECTOR3 start,D3DXVECTOR3 control1,D3DXVECTOR3 control2,D3DXVECTOR3 end)
{
	m_type=type;
	m_StartPos=start;
	m_Control1=control1;
	m_Control2=control2;
	m_EndPos=end;
}

void Path::ShutDown()
{
	if(m_pNext)
	{
		m_pNext->ShutDown();
		delete m_pNext;
		m_pNext=NULL;
	}

}//注意链表还剩头部没有delete



Route::Route()
{
	m_pPath=NULL;
	m_StartTime=0;
}

Route::~Route()
{
	ShutDown();
}

bool Route::AddPath(int type,D3DXVECTOR3 start,D3DXVECTOR3 control1,D3DXVECTOR3 control2,D3DXVECTOR3 end)
{
	Path *ptr=m_pPath;

	if(m_pPath)
	{
		while (ptr->m_pNext)
		{
			ptr=ptr->m_pNext;
		}
		ptr->m_pNext=new Path(type,start,control1,control2,end);
		ptr->m_pNext->m_StartLenth=ptr->m_TotalLenth+ptr->m_StartLenth;

		ptr=ptr->m_pNext;
	}
	else
	{
		m_pPath=new Path(type,start,control1,control2,end);
		m_pPath->m_StartLenth=0;

		ptr=m_pPath;
	}

	if(type==STRAIGHT_PATH)
	{
		ptr->m_TotalLenth=D3DXVec3Length(&(end-start));
	}
	else if (type==CURVE_PATH)
	{
		float lenghth01=D3DXVec3Length(&(control1-start));
		float lenghth12=D3DXVec3Length(&(control2-control1));
		float lenghth23=D3DXVec3Length(&(end-control2));
		float lenghth03=D3DXVec3Length(&(end-start));
		ptr->m_TotalLenth=(lenghth01+lenghth12+lenghth23+lenghth03)*0.5f;
	}
	else
		return false;


	return true;
}

void Route::GetPosition(float S,D3DXVECTOR3 &objPos)
{
	if(!m_pPath)	return;
	Path *ptr=m_pPath;

	if(m_StartTime==0) m_StartTime=(float)timeGetTime();
	
	while (ptr)
	{
		if(S>=ptr->m_StartLenth && S<ptr->m_StartLenth+ptr->m_TotalLenth)
		{
			S-=ptr->m_StartLenth;
			float scalar=S/ptr->m_TotalLenth;

			if(ptr->m_type==STRAIGHT_PATH)
				objPos=CalcStriaghtPos(ptr->m_StartPos,ptr->m_EndPos,scalar);
			else
				objPos=CalcBezierCurvePos(ptr->m_StartPos,ptr->m_Control1,ptr->m_Control2,ptr->m_EndPos,scalar);

			break;
		}

		if(!ptr->m_pNext)
		{	
			m_StartTime=(float)timeGetTime();
			objPos=m_pPath->m_StartPos;
		}

		ptr=ptr->m_pNext;
	}

}

D3DXVECTOR3 Route::CalcStriaghtPos(D3DXVECTOR3 start,D3DXVECTOR3 end,float scalar)
{
	D3DXVECTOR3 out=(end-start)*scalar+start;

	return out;
}

D3DXVECTOR3 Route::CalcBezierCurvePos(D3DXVECTOR3 start,D3DXVECTOR3 control1,D3DXVECTOR3 control2,D3DXVECTOR3 end,float scalar)
{
	D3DXVECTOR3 out;
	out=start*(1.0f-scalar)*(1.0f-scalar)*(1.0f-scalar)
		+control1*3.0f*scalar*(1.0f-scalar)*(1.0f-scalar)
		+control2*3.0f*scalar*scalar*(1.0f-scalar)
		+end*scalar*scalar*scalar;

	return out;
}

void Route::ShutDown()
{
	if(m_pPath)
	{
		m_pPath->ShutDown();

		delete m_pPath;
		m_pPath=NULL;
	}
}