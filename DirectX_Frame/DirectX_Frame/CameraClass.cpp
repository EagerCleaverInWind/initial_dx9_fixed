
#include "CameraClass.h"

Camera::Camera(LPDIRECT3DDEVICE9 pd3dDevice, CameraType type)
{
	m_pd3dDevice=pd3dDevice;
	m_Type=type;
	m_up=D3DXVECTOR3(0,1,0);
	m_look=D3DXVECTOR3(0,0,1);
	m_right=D3DXVECTOR3(1,0,0);
	m_position=D3DXVECTOR3(0,0,-250);
	Update();

	D3DXMatrixPerspectiveFovLH(&m_ProjMatrix,D3DX_PI/4.0f,(float)(SCREEN_WIDTH/SCREEN_HEIGHT),1.0f,30000.0f);
	//日了狗了，一开始错自动补全成 D3DXMatrixPerspectiveLH()，看东西成了扁的……
	m_pd3dDevice->SetTransform(D3DTS_PROJECTION,&m_ProjMatrix);

}

void	Camera::CalculateViewMatrix()
{
	//规范化m_up，m_look和m_right
	D3DXVec3Normalize(&m_look,&m_look);
	D3DXVec3Cross(&m_up,&m_look,&m_right);//注意D3D坐标系满足的是左手定则
	D3DXVec3Normalize(&m_up,&m_up);//LANDOBJECT左右转动后，”脱轨“的是UpVector
	D3DXVec3Cross(&m_right,&m_up,&m_look);
	D3DXVec3Normalize(&m_right,&m_right);

	//第一行			RX	UX	LX	0
	m_ViewMatrix._11=m_right.x;			
	m_ViewMatrix._12=m_up.x;
	m_ViewMatrix._13=m_look.x;			
	m_ViewMatrix._14=0.0f;
	//第二行			RY	UY	LY		0
	m_ViewMatrix._21=m_right.y;
	m_ViewMatrix._22=m_up.y;	
	m_ViewMatrix._23=m_look.y;	
	m_ViewMatrix._24=0.0f;
	//第三行			RZ	UZ	LZ		0
	m_ViewMatrix._31=m_right.z;
	m_ViewMatrix._32=m_up.z;	
	m_ViewMatrix._33=m_look.z;	
	m_ViewMatrix._34=0.0f;
	//第四行			-P*R	-P*U	-P*L	1
	m_ViewMatrix._41=-D3DXVec3Dot(&m_position,&m_right);
	m_ViewMatrix._42=-D3DXVec3Dot(&m_position,&m_up);
	m_ViewMatrix._43=-D3DXVec3Dot(&m_position,&m_look);
	m_ViewMatrix._44=1.0f;
}


void  Camera::RotationUpVec(float angle)
{
	D3DXMATRIX  R;
	if(m_Type==LANDOBJECT)
		D3DXMatrixRotationY(&R,angle);
	else
		D3DXMatrixRotationAxis(&R,&m_up,angle);
	D3DXVec3TransformCoord(&m_right,&m_right,&R);
	D3DXVec3TransformCoord(&m_look,&m_look,&R);
}

void  Camera::RotationLookVec(float angle)
{
	D3DXMATRIX  R;
	if (m_Type==AIRCRAFT)
	{
		D3DXMatrixRotationAxis(&R,&m_look,angle);
		D3DXVec3TransformCoord(&m_right,&m_right,&R);
		D3DXVec3TransformCoord(&m_up,&m_up,&R);
	}
}

void  Camera::RotationRightVec(float angle)
{
	D3DXMATRIX  R;
	D3DXMatrixRotationAxis(&R,&m_right,angle);
	D3DXVec3TransformCoord(&m_up,&m_up,&R);
	D3DXVec3TransformCoord(&m_look,&m_look,&R);
}

void Camera::Update()
{
	CalculateViewMatrix();
	UpdateWorldFrustumPlanes();
}

void	Camera::SetViewProj(D3DXMATRIX *pMatrix)
{
	m_pd3dDevice->SetTransform(D3DTS_VIEW,&m_ViewMatrix);

	if(pMatrix)
	{
		m_ProjMatrix=*pMatrix;
		m_pd3dDevice->SetTransform(D3DTS_PROJECTION,&m_ProjMatrix);
	}
	
}

void Camera::UpdateWorldFrustumPlanes()
{
	D3DXMATRIX VP=m_ViewMatrix*m_ProjMatrix;

	//Extract the frustum planes in world space from VP
	D3DXVECTOR4 col0(VP(0,0), VP(1,0), VP(2,0), VP(3,0));
	D3DXVECTOR4 col1(VP(0,1), VP(1,1), VP(2,1), VP(3,1));
	D3DXVECTOR4 col2(VP(0,2), VP(1,2), VP(2,2), VP(3,2));
	D3DXVECTOR4 col3(VP(0,3), VP(1,3), VP(2,3), VP(3,3));
	// Planes face inward.
	m_FrustumPlanes[0] = (D3DXPLANE)(col2);        // near
	m_FrustumPlanes[1] = (D3DXPLANE)(col3 - col2); // far
	m_FrustumPlanes[2] = (D3DXPLANE)(col3 + col0); // left
	m_FrustumPlanes[3] = (D3DXPLANE)(col3 - col0); // right
	m_FrustumPlanes[4] = (D3DXPLANE)(col3 - col1); // top
	m_FrustumPlanes[5] = (D3DXPLANE)(col3 + col1); // bottom

	for(int i = 0; i < 6; i++)
		D3DXPlaneNormalize(&m_FrustumPlanes[i], &m_FrustumPlanes[i]);
}

bool Camera::IsVisible(const BoundingBox &box)
{
	// Test assumes frustum planes face inward.

	D3DXVECTOR3 P;
	D3DXVECTOR3 Q;

	// PQ forms diagonal most closely aligned with plane normal.
	// For each frustum plane, find the box diagonal (there are four main
	// diagonals that intersect the box center point) that points in the
	// same direction as the normal along each axis (i.e., the diagonal 
	// that is most aligned with the plane normal).  Then test if the box
	// is in front of the plane or not.
	for(int i = 0; i < 6; ++i)
	{
		// For each coordinate axis x, y, z...
		for(int j = 0; j < 3; ++j)
		{
			// Make PQ point in the same direction as the plane normal on this axis.
			if( m_FrustumPlanes[i][j] >= 0.0f )
			{
				P[j] = box._min[j];
				Q[j] = box._max[j];
			}
			else 
			{
				P[j] = box._max[j];
				Q[j] = box._min[j];
			}
		}

		// If box is in negative half space, it is behind the plane, and thus, completely
		// outside the frustum.  Note that because PQ points roughly in the direction of the 
		// plane normal, we can deduce that if Q is outside then P is also outside--thus we
		// only need to test Q.
		if( D3DXPlaneDotCoord(&m_FrustumPlanes[i], &Q) < 0.0f  ) // outside
			return false;
	}
	return true;
}

Camera::~Camera()
{
	
}