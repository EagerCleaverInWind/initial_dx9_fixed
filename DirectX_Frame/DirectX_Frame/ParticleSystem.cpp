#include "ParticleSystem.h"

//*****************************************************************************
// Base System
//***************
PSystem::PSystem():m_pDevice(NULL),m_pVertexBuf(NULL),m_pTex(NULL)
{

}
//各数量尺寸的设定交给子代

PSystem::~PSystem()
{
	if(m_pVertexBuf)
	{
		m_pVertexBuf->Release();
		m_pVertexBuf=NULL;
	}
	if(m_pTex)
	{
		m_pTex->Release();
		m_pTex=NULL;
	}
}

bool PSystem::Init(LPDIRECT3DDEVICE9 device,wchar_t *texFileName)
{
	m_pDevice=device;

	HRESULT	 hr=0;
	hr=m_pDevice->CreateVertexBuffer(m_vbSize*sizeof(Particle),D3DUSAGE_DYNAMIC|D3DUSAGE_POINTS|D3DUSAGE_WRITEONLY,Particle::FVF,D3DPOOL_DEFAULT,&m_pVertexBuf,0);
	if(FAILED(hr)) 
		return false;
	//一开始写的是if(!m_pDevice->CreateVertexBuffer(…))	return false;
	//调试时发现竟然会将S_OK转为false！！

	//if(!D3DXCreateTextureFromFile(m_pDevice,texFileName,&m_pTex))
	//return false;
		D3DXCreateTextureFromFile(m_pDevice,texFileName,&m_pTex);
		
	
	return true;
}

void PSystem::Reset()
{
	std::list<Attribute>::iterator	i;
	for(i=m_Particles.begin();i!=m_Particles.end();i++)
	{
		ResetParticle(&(*i));
	}
}

void PSystem::AddParticle()
{
	Attribute attribute;
	ResetParticle(&attribute);
	m_Particles.push_back(attribute);
}

void PSystem::PreRender()
{
	m_pDevice->SetRenderState(D3DRS_LIGHTING,false);
	//	about point sprite:
	m_pDevice->SetRenderState(D3DRS_POINTSPRITEENABLE,true);
	m_pDevice->SetRenderState(D3DRS_POINTSCALEENABLE,true);
	m_pDevice->SetRenderState(D3DRS_POINTSIZE,FtoDw(m_ParSize));
	m_pDevice->SetRenderState(D3DRS_POINTSIZE_MIN,FtoDw(0.0f));
	m_pDevice->SetRenderState(D3DRS_POINTSCALE_A,FtoDw(0.0f));
	m_pDevice->SetRenderState(D3DRS_POINTSCALE_B,FtoDw(0.0f));
	m_pDevice->SetRenderState(D3DRS_POINTSCALE_C,FtoDw(1.0f));
	//use alpha from texture:
	m_pDevice->SetTextureStageState(0,D3DTSS_ALPHAARG1,D3DTA_TEXTURE);
	m_pDevice->SetTextureStageState(0,D3DTSS_ALPHAOP,D3DTOP_SELECTARG1);
	//set transparency mode:
	m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE,true);
	m_pDevice->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_SRCALPHA);
	m_pDevice->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);
}

void PSystem::PostRender()
{
	m_pDevice->SetRenderState(D3DRS_LIGHTING,true);
	m_pDevice->SetRenderState(D3DRS_POINTSPRITEENABLE,false);
	m_pDevice->SetRenderState(D3DRS_POINTSCALEENABLE,false);
	m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE,false);
}

void PSystem::Render(D3DMATRIX *pMatWorld)
{
	if(IsEmpty())
		return;

	PreRender();
	if(!pMatWorld)
	{
		D3DXMATRIX world;
		D3DXMatrixTranslation(&world,0.0f,0.0f,0.0f);
		pMatWorld=&world;
		//为了不受外部其他渲染所设置的世界变换的影响
	}
	
	m_pDevice->SetTransform(D3DTS_WORLD,pMatWorld);
	

	m_pDevice->SetTexture(0,m_pTex);
	m_pDevice->SetFVF(Particle::FVF);
	m_pDevice->SetStreamSource(0,m_pVertexBuf,0,sizeof(Particle));

	if(m_vbOffset>=m_vbSize)
		m_vbOffset=0;
	Particle *v=0;
	m_pVertexBuf->Lock(m_vbOffset*sizeof(Particle),
		m_vbBatchSize*sizeof(Particle),
		(void**)&v,
		m_vbOffset?D3DLOCK_NOOVERWRITE : D3DLOCK_DISCARD);

	DWORD numParticlesInBatch=0;
	std::list<Attribute>::iterator i;
	for(i=m_Particles.begin();i!=m_Particles.end();i++)
	{
		if(i->_isAlive)
		{
			v->pisition=i->_position;
			v->color=i->_color;
			v++;

			numParticlesInBatch++;
			if(numParticlesInBatch==m_vbBatchSize)
			{
				m_pVertexBuf->Unlock();
				m_pDevice->DrawPrimitive(D3DPT_POINTLIST,m_vbOffset,m_vbBatchSize);

				m_vbOffset+=m_vbBatchSize;
				if(m_vbOffset>m_vbSize)
					m_vbOffset=0;
				m_pVertexBuf->Lock(m_vbOffset*sizeof(Particle),m_vbBatchSize*sizeof(Particle),(void**)&v,m_vbOffset?D3DLOCK_NOOVERWRITE : D3DLOCK_DISCARD);
				numParticlesInBatch=0;
			}
		}
	}

	m_pVertexBuf->Unlock();
	if(numParticlesInBatch)
	{
		m_pDevice->DrawPrimitive(D3DPT_POINTLIST,m_vbOffset,numParticlesInBatch);
		m_vbOffset+=numParticlesInBatch;
	}

	PostRender();
}

void PSystem::RemoveDeadParticles()
{
	std::list<Attribute>::iterator	i;
	for(i=m_Particles.begin();i!=m_Particles.end();i++)
	{
		if(!i->_isAlive)
		{
			i=m_Particles.erase(i);
			i--;
		}
	}
}

bool PSystem::IsDead()
{
	std::list<Attribute>::iterator i;
	for(i=m_Particles.begin();i!=m_Particles.end();i++)
	{
		if(i->_isAlive)
			return false;
	}
	return true;
}


//*****************************************************************************
// Snow System
//***************
Snow::Snow(BoundingBox *boundingBox,int numParticles)
{
	m_bouBox=*boundingBox;
	m_ParSize=0.25f;
	m_vbSize=2048;
	m_vbOffset=0;
	m_vbBatchSize=512;

	for(int i=0;i<numParticles;i++)
		AddParticle();
}

void Snow::ResetParticle(Attribute *attribute)
{
	attribute->_isAlive=true;

	GetRandomVector(&(attribute->_position),&m_bouBox._min,&m_bouBox._max);
	attribute->_position.y=m_bouBox._max.y;

	attribute->_velocity.x=GetRandomFloat(0.0f,1.0f)*-3.0f;
	attribute->_velocity.y=GetRandomFloat(0.0,1.0)*-10.0f;//一开始没加负号，一整天各种调试还是黑屏，直到在Update的if设置了两个断点T_T
	attribute->_velocity.z=0.0f;

	attribute->_color=D3DCOLOR_XRGB(255,255,255);
}

void Snow::Update(float timeDelta)
{
	std::list<Attribute>::iterator i;
	for(i=m_Particles.begin();i!=m_Particles.end();i++)
	{
			i->_position+=i->_velocity*timeDelta;
			if(!m_bouBox.IfPointInside(i->_position))
				ResetParticle(&(*i));
	}
}


//*****************************************************************************
// FireWork System
//***************
FireWork::FireWork(D3DXVECTOR3 *origin,int numParticles)
{
	m_iniPos=*origin;
	m_ParSize=0.9f;
	m_vbSize=2048;
	m_vbOffset=0;
	m_vbBatchSize=512;

	for(int i=0;i<numParticles;i++)
		AddParticle();
}

void	FireWork::ResetParticle(Attribute *attribute)
{
	attribute->_isAlive=true;
	attribute->_position=m_iniPos;
	
	GetRandomVector(&attribute->_velocity,&D3DXVECTOR3(-1.0f,-1.0f,-1.0f),&D3DXVECTOR3(1.0f,1.0f,1.0f));
	D3DXVec3Normalize(&attribute->_velocity,&attribute->_velocity);//为了球形，各方向速度大小须相同
	attribute->_velocity*=100.0f;

	attribute->_color=D3DXCOLOR(GetRandomFloat(0.0f,1.0f),GetRandomFloat(0.0f,1.0f),GetRandomFloat(0.0f,1.0f),1.0f);

	attribute->_age=0.0f;
	attribute->_lifetime=2.0f;

}

void	FireWork::Update(float timeDelta)
{
	std::list<Attribute>::iterator i;
	for(i=m_Particles.begin();i!=m_Particles.end();i++)
	{
		if(i->_isAlive)
		{
			i->_position+=i->_velocity*timeDelta;
			i->_age+=timeDelta;

			if(i->_age>=i->_lifetime)
				i->_isAlive=false;
		}
	}

}

void FireWork::PreRender()
{
	PSystem::PreRender();

	m_pDevice->SetRenderState(D3DRS_SRCBLEND,D3DBLEND_ONE);
	m_pDevice->SetRenderState(D3DRS_DESTBLEND,D3DBLEND_ONE);

	// read, but don't write particles to z-buffer
	m_pDevice->SetRenderState(D3DRS_ZWRITEENABLE,false);
}

void FireWork::PostRender()
{
	PSystem::PostRender();

	m_pDevice->SetRenderState(D3DRS_ZWRITEENABLE,true);
}


//*****************************************************************************
//ParticleGun System
//***************
ParticleGun::ParticleGun(Camera *camera,D3DXCOLOR laserColor)
{
	m_pCamera=camera;
	m_LaserColor=laserColor;
	m_ParSize =0.8f;
	m_vbBatchSize =512;
	m_vbOffset  =0;  
	m_vbSize =2048; 
}

void ParticleGun::ResetParticle(Attribute *attribute)
{
	attribute->_isAlive=true;

	m_pCamera->GetPosition(&attribute->_position);
	attribute->_position.y-=1.0f;

	m_pCamera->GetLookDirection(&attribute->_velocity);
	attribute->_velocity*=100.0f;

	attribute->_color=m_LaserColor;

	attribute->_age=0.0f;
	attribute->_lifetime=1.0f;
}

void ParticleGun::Update(float timeDelta)
{
	std::list<Attribute>::iterator i;
	for(i=m_Particles.begin();i!=m_Particles.end();i++)
	{
		i->_position+=i->_velocity*timeDelta;
		i->_age+=timeDelta;
		if(i->_age>=i->_lifetime)
			i->_isAlive=false;	
	}
	RemoveDeadParticles();
}