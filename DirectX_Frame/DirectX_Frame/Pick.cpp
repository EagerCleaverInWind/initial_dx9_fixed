#include "Pick.h"

void Pick::ComputeBoundingSphereFromMesh(ID3DXMesh *mesh,D3DXVECTOR3 *center,float *radius)
{
	BYTE *v=0;
	mesh->LockVertexBuffer(0,(void**)&v);
	D3DXComputeBoundingSphere((D3DXVECTOR3*)v,mesh->GetNumVertices(),D3DXGetFVFVertexSize(mesh->GetFVF()),center,radius);
	mesh->UnlockVertexBuffer();
}
void Pick::ComputeBoundingBoxFromMesh(ID3DXMesh *mesh,D3DXVECTOR3 *min,D3DXVECTOR3 *max)
{
	BYTE *v=0;
	mesh->LockVertexBuffer(0,(void**)&v);
	D3DXComputeBoundingBox((D3DXVECTOR3*)v,mesh->GetNumVertices(),D3DXGetFVFVertexSize(mesh->GetFVF()),min,max);
	mesh->UnlockVertexBuffer();
}

void Pick::UpdatePointP(float x,float y)
{
	D3DVIEWPORT9 vp;
	m_pd3dDevice->GetViewport(&vp);

	D3DMATRIX proj;
	m_pd3dDevice->GetTransform(D3DTS_PROJECTION,&proj);

	m_ViewCoordinateP.x=(2.0f*x/vp.Width-1.0f)/proj._11;
	m_ViewCoordinateP.y=(-2.0f*y/vp.Height+1.0f)/proj._22;
	m_ViewCoordinateP.z=1.0f;

	//calculate ray in view coordinate:
	m_ViewCoordinateRay._direction=m_ViewCoordinateP;
	//calculate ray in world coordinate
	TransformRay();
}

void Pick::TransformRay()
{
	D3DXMATRIX view;
	m_pd3dDevice->GetTransform(D3DTS_VIEW,&view);

	D3DXMATRIX viewInverse;
	D3DXMatrixInverse(&viewInverse,0,&view);

	D3DXVec3TransformCoord(&m_WorldCoordinateRay._origin,&m_ViewCoordinateRay._origin,&viewInverse);
	D3DXVec3TransformNormal(&m_WorldCoordinateRay._direction,&m_ViewCoordinateRay._direction,&viewInverse);
}

bool Pick::RaySphereIntersectDetection(BoundingSphere *sphere)
{
	D3DXVECTOR3 v=m_WorldCoordinateRay._origin-sphere->_center;

	float B=2.0f*D3DXVec3Dot(&m_WorldCoordinateRay._direction,&v);
	float C=D3DXVec3Dot(&v,&v)-(sphere->_radius*sphere->_radius);

	float discriminate=B*B-4.0F*C;
	if(discriminate<0.0f)
		return false;

	discriminate=sqrt(discriminate);
	float s0=(-B+discriminate)/2.0f;
	float s1=(-B-discriminate)/2.0f;

	if(s0>=0.0f||s1>=0.0f)
		return true;

	return false;
}
bool Pick::RayBoxIntersectDetection(BoundingBox *box)
{
	//本以为用3*2次D3DXIntersectTri()就可以了……
	return false;
}

